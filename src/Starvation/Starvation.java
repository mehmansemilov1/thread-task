package Starvation;

public class Starvation {

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            while (true) {
                System.err.println("Thread 1 worked");
            }
        });

        Thread thread2 = new Thread(() -> {
            System.err.println("Thread 2 worked");
        });

        thread1.start();
        thread2.start();
    }
}
